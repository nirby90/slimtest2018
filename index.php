<?php
require "vendor/autoload.php";
require "bootstrap.php";


use Chatter\Models\Message;
use Chatter\Middleware\Logging as ChatterLogging;


$app = new \Slim\App();
$app->add(new ChatterLogging);


$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cid}/products/{pid}', function($request, $response,$args){
   return $response->write('Hi customer '.$args['cid']. 'You purchased product numner '.$args['pid']);
});

$app->get('/messages', function($request, $response,$args){
   $_massge = new Message();
   $messages = $_massge->all();
    $paylod = [];
    foreach ($messages as $msg){
        $paylod[$msg->id] = ['body'=>$msg->body,
                             'user_id'=>$msg->user_id,
                             'created_at' => $msg->created_at];
    }
    return $response->withStatus(200)->withJson($paylod);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = new Message();
    $_message->body =  $message;
    $_message->user_id = 1; 
    $_message->save();
    if($_message->id){
       $payload =  ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
    }else{
        return $response->withStatus(400);
    }
});

$app->delete('/messages/{message_id}', function($request, $response, $args){
    $message = Message::find($args['message_id']);
    $message->delete();
    if($message->exists){
        return $response->withStatus(400); 
    }else{
        return $response->withStatus(204); 
    }
});

$app->put('/messages/{message_id}', function($request, $response, $args){
     $body = $request->getParsedBodyParam('message','');
     $_message = Message::find($args['message_id']);
     $_message->body = $body;

    if($_message->save()){
         $payload = ['message_id' => $_message->id,
         'message_uri' => '/messages/'.$_message->id];
         return $response->withStatus(200)->withJson($payload);
     }else{
         return $response->withStatus(400); 
     }
 });
 $app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    //die(var_dump($payload));
    
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);
});




$app->run();
